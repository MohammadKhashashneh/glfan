/**
* main.cpp:
*
* Main glfan application
*
* The MIT License (MIT)
* 2022 Mohammad Khashashneh mohammad.rasmi@gmail.com
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

#include "fan.hpp"
#include <iostream>

void displayFunc(void) {
	fan::onDisplay();
}

void reshapeFunc(GLint w, GLint h) {
	fan::onResize(w, h);
}

void inputUpFunc(GLint key, GLint x, GLint y) {
	if(GLUT_KEY_LEFT == key)
		fan::addFanSpeed(50.0f);
	if(GLUT_KEY_RIGHT == key)
		fan::addFanSpeed(-50.0f);
}

void glutSetup(
		int* argc, char* argv[],
		int width, int height,
		int xPos, int yPos,
		const char* name,
		void (*displayFunc)(GLvoid),
		void (*reshapeFunc)(GLint, GLint),
		void (*inputUpFunc)(GLint, GLint, GLint))
{
	glutInit(argc, argv);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(xPos, yPos);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutCreateWindow(name);
	glutDisplayFunc(displayFunc);
	glutReshapeFunc(reshapeFunc);
	glutSpecialUpFunc(inputUpFunc);
}

int main(int argc, char *argv[]) {
	glutSetup(&argc, argv,
		fan::windowWidth, fan::windowHeight,
		fan::windowXPosition, fan::windowYPosition,
		fan::appName.c_str(),
		displayFunc, reshapeFunc, inputUpFunc
	);

	if(!fan::init())
		return 1;

	glutMainLoop();
	return 0;
}
