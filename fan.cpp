/**
* fan.cpp:
*
* The glfan application functions and draw instructions
*
* The MIT License (MIT)
* 2022 Mohammad Khashashneh mohammad.rasmi@gmail.com
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

#include "fan.hpp"
#include <cmath>
#include <iostream>

namespace fan {

	void addFanSpeed(GLfloat value) {
		fanSpeed += value;
	}

	void drawAxis() {
		glEnable(GL_LINE_STIPPLE);
		glLineStipple(1, 0x5555);
		glBegin(GL_LINES);

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-clipWidth, 0.0f, 0.0f);
		glVertex3f(clipWidth, 0.0f, 0.0f);
		glVertex3f(0.0f, -clipHeight, 0.0f);
		glVertex3f(0.0f, clipHeight,  0.0f);
		glVertex3f(0.0f, 0.0f, -clipDepth);
		glVertex3f(0.0f, 0.0f, clipDepth);

		glEnd();
		glDisable(GL_LINE_STIPPLE);
	}

	void drawFanTrunk() {
		glColor3f(0.0f, 0.4f, 0.0f);
		glRecti(-10.0f, 0.0f, 10.0f, -clipHeight);
	}

	void drawStar(GLfloat radius, GLfloat spacing) {
		GLfloat angle = GL_PI;
		glBegin(GL_LINES);
		for(size_t i = 180; i > 0; i--) {
			glVertex3d(0.0f, 0.0f, zAxis);
			GLfloat x = radius * std::sin(angle);
			GLfloat y = radius * std::cos(angle);
			glVertex3f(x, y, zAxis);
			angle -= spacing;
		}
		glEnd();
	}

	void drawCircle(GLfloat radius) {
		const GLfloat disp = GL_PI / 90;
		GLfloat angle = GL_PI;
		glBegin(GL_LINE_STRIP);
		for(size_t i = 180; i > 0; i--) {
			GLfloat x = radius * std::sin(angle);
			GLfloat y = radius * std::cos(angle);
			glVertex3f(x, y, zAxis);
			angle -= disp;
		}
		glEnd();
	}

	void drawFanBackFrame() {
		glColor3f(0.0f, 0.0f, 0.4f);
		drawStar(110.0f, GL_PI / 30);
		drawCircle(110.0f);
		glColor3f(0.0f, 0.4f, 0.4f);
		drawCircle(120.0f);
	}

	void drawFanFrontFrame() {
		glColor3f(0.4f, 0.0f, 0.4f);
		glPushMatrix();
		glRotatef(10.0f, 0.0f, 0.0f, 1.0f);
		drawStar(110.0f, GL_PI / 30);
		glPopMatrix();
	}

	void drawFanBlades() {
		GLfloat x, y;
		GLfloat disp = GL_PI / 3.0f;
		GLfloat angle = 0;

		glColor3f(1.0f, 0.5f, 0.0f);
		glBegin(GL_TRIANGLES);

		for(size_t i = 0; i < 3; i++) {
			glVertex3f(0.0f, 0.0f, zAxis);

			x = 100.0f * std::sin(angle);
			y = 100.0f * std::cos(angle);
			glVertex3f(x, y, zAxis);

			angle += disp;
			x = 100.0f * std::sin(angle);
			y = 100.0f * std::cos(angle);
			glVertex3f(x, y, zAxis);

			angle += disp;
		}
		glEnd();
	}

	void drawFan() {
		drawFanTrunk();
		drawFanBackFrame();
		glPushMatrix();
		glRotatef(fanRot, 0.0f, 0.0f, 1.0f);
		drawFanBlades();
		glPopMatrix();
		drawFanFrontFrame();
	}

	void onDisplay(void) {
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glClear(GL_COLOR_BUFFER_BIT);

		//drawAxis();
		drawFan();

		glutSwapBuffers();
	}

	void onResize(int width, int height) {
		if(width == 0)
			width = 1;
		if(height == 0)
			height = 1;

		GLdouble wRatio = (GLdouble)width / height;
		GLdouble hRatio = (GLdouble)height / width;

		windowWidth = width;
		windowHeight = height;

		glViewport(0, 0, windowWidth, windowHeight);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		if(wRatio >= hRatio)
			glOrtho(-clipWidth, clipWidth,
					-clipHeight * hRatio, clipHeight * hRatio,
					-clipDepth, clipDepth);
		else
			glOrtho(-clipWidth * wRatio, clipWidth * wRatio,
					-clipHeight, clipHeight,
					clipDepth, -clipDepth);
	}


	void timerCallback(int value) {
		auto now = std::chrono::steady_clock::now();
		std::chrono::duration<double> elapsed = now - start;
		start = now;
		GLfloat slowdown = elapsed.count() * 10.0f;

		if(fanSpeed < 0) {
			fanSpeed += slowdown;
			if(fanSpeed >= -slowdown)
				fanSpeed = 0;
		} else if(fanSpeed > 0) {
			fanSpeed -= slowdown;
			if(fanSpeed <= slowdown)
				fanSpeed = 0;
		}

		fanRot += fanSpeed * GL_PI * elapsed.count();
		if(360.0f <= fanRot)
			fanRot = fanRot - 360.0f;

		glutPostRedisplay();
		glutTimerFunc(frameRate, timerCallback, 0);
	}

	bool init() {
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		start = std::chrono::steady_clock::now();
		glutTimerFunc(frameRate, timerCallback, 0);
		return true;
	}
}
