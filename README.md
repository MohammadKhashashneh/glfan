# GL Fan
a simple OpenGL program that draws a spinning fan, created to refresh 
my memory with OpenGL programming.

### Requirments
 - CMake 3.5+ and Make
 - GL, GLUT and Glew development packages installed
 - C++ compiler
 - Linux, but since this uses CMake and Glut, windows with Mingw-w64 should
 also be possible with a little extra work.

### To build and run
 - `mkdir buuld`
 - `cmake PATH_TO_glfan_SOURCE_CODE`
 - `make`
 - `./glfan`

### Controls
 - Left key spins to the left
 - Right key spins to the right

### TODOs
 - [ ] Adding cross compiler instructions to build for windows and Mac
