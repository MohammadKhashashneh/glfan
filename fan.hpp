/**
* fan.hpp:
*
* The glfan main header
*
* The MIT License (MIT)
* 2022 Mohammad Khashashneh mohammad.rasmi@gmail.com
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include <string>
#include <chrono>
#include <GL/glut.h>
#include <GL/gl.h>

namespace fan {
	static const std::string appName = "glfan";
	static const GLfloat GL_PI = 3.1415f;
	static const GLfloat frameRate = 16.6666666667f;
	static const int windowXPosition = 50;
	static const int windowYPosition = 50;
	static const int clipDepth = 500;
	static int windowWidth = 1280;
	static int windowHeight = 720;
	static int clipWidth = 500;
	static int clipHeight = 500;
	static std::chrono::time_point<std::chrono::steady_clock> start;
	static GLfloat zAxis = 0.0f;
	static GLfloat fanRot = 0.0f;
	static GLfloat fanSpeed = 200.0f;

	void onDisplay(void);
	void onResize(int width, int height);
	void addFanSpeed(GLfloat value);
	bool init();
}
